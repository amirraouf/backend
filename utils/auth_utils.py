import datetime
import time
import uuid
import binascii
import os
from Crypto.Cipher.AES import AESCipher
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import authenticate


conn = cache.get('authentication')


def generate_hash():
    """
    Generates a random hash using the uuid4 module
    """

    return str(uuid.uuid4().get_hex())


def create_client_access_token(key, secret, expires_after_days=30):
    """
    Creates an access_token for an API client
    based on the key, secret and timestamp.

    Encryption Algo used from krypt script.
    """

    ## First we try to get the token from the redis cache
    ## if the token exists we directly return the token
    tdata = conn.get('{0}{1}{2}'.format(key, settings.DEFAULT_SEP, secret))
    if tdata:
        token = tdata.get('token')
        expires_on = tdata.get('expires_on')

        return token, expires_on

    ## If token does not exist in the cache create a new token
    ## and then put it in the cache
    now = datetime.datetime.now()
    expires_on = now + datetime.timedelta(expires_after_days)
    cache_expire = int((expires_on - now).total_seconds())
    expires_on = expires_on.strftime(settings.DATE_FORMAT)

    raw_token_key = '{0}{1}{2}{3}{4}'.format(key, settings.DEFAULT_SEP, secret,
                                    settings.DEFAULT_SEP, expires_on)
    cipher = AESCipher()
    token = cipher.encrypt(raw_token_key)

    ## Save the newly created token in the cache
    ## and expire the entry on expires_on value
    cache_map = {'token': token, 'expires_on': expires_on}
    conn.set('{0}{1}{2}'.format(key, settings.DEFAULT_SEP, secret), cache_map,
             timeout=cache_expire)

    return token, expires_on


def client_access_token_valid(access_token):
    """
    Returns a boolean value of whether or not
    a given client access token is valid
    """

    cipher = AESCipher()

    try:
        raw_token = cipher.decrypt(access_token)
    except (TypeError, ValueError, Exception) as err:
        raw_token = ""

    raw_split = raw_token.split(settings.DEFAULT_SEP)

    if len(raw_split) == 3:
        key, secret, expires_on = raw_split
        tdata = conn.get('%s%s%s' % (key, settings.DEFAULT_SEP, secret))

        if tdata:
            return tdata.get('token') == access_token

    return False


def generate_api_key():
    return binascii.hexlify(os.urandom(20)).decode()
