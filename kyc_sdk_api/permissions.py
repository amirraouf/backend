from users.service_provider.models.api_key import APIKey
from rest_framework import permissions


class HasAPIAccess(permissions.BasePermission):
    message = 'Invalid or missing API Key.'

    def has_permission(self, request, view):
        api_key = request.META.get('HTTP_X_API_KEY', '')
        issuer = request.META.get('HTTP_ISSUER', '')
        return APIKey.objects.filter(key=api_key, entity__name=issuer).exists()


class ClientObjectAccess(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # TODO
        return True
