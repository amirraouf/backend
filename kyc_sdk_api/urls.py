from django.conf.urls import include, url
from kyc_sdk_api.v1.urls import urls as v1_urls
from rest_framework_swagger.views import get_swagger_view
from kyc_sdk_api.rest_jwt.views import obtain_jwt_token as kyc_obtain_jwt_token, refresh_jwt_token, verify_jwt_token

schema_view = get_swagger_view(title='Pastebin API')
api_urls = [
    url(r'^v1/', include(v1_urls)),
    url(r'^docs/v1/$', schema_view),
    url(r'^kyc-api-token-auth/', kyc_obtain_jwt_token),
    url(r'^kyc-refresh_jwt_token/', refresh_jwt_token),
    url(r'^kyc-verify_jwt_token/', verify_jwt_token),
]

