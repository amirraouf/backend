from django.conf.urls import url, include

from kyc_sdk_api.v1.re_kyc.urls import urls as rekyc_urls

urls = [
    url(r'^re-kyc/', include(rekyc_urls))
]
