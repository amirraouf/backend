from rest_framework import renderers
from rest_framework_swagger import renderers as swagger_renderers
from rest_framework.schemas import SchemaGenerator
from rest_framework.response import Response

from kyc_sdk_api.rest_jwt.authentication import JSONWebTokenAuthentication
from kyc_sdk_api.permissions import HasAPIAccess, ClientObjectAccess
from rest_framework.utils.serializer_helpers import ReturnDict


class StatusCodeSerializerMixin(object):
    @property
    def errors(self):
        ret = super(StatusCodeSerializerMixin, self).errors
        if len(ret):
            ret.update({'code': '3111', 'message': 'check errors field'})
        return ReturnDict(ret, serializer=self)


class APIMixin(object):
    renderer_classes = (renderers.JSONRenderer,
                        swagger_renderers.OpenAPIRenderer,
                        swagger_renderers.SwaggerUIRenderer)
    http_method_names = ['get', 'post', 'options']

    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema(request=request)

        return Response(schema)

    def validate_data(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        return data


class PermissionAPIMixin(APIMixin):
    permission_classes = (HasAPIAccess,)


class JWTAuthAPIMixin(APIMixin):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (ClientObjectAccess, )
