import json

from django.http import JsonResponse
from rest_framework import parsers
from rest_framework.views import APIView

from kyc_sdk_api.v1.mixins import PermissionAPIMixin, JWTAuthAPIMixin
from users.client.models import Client
from users.service_provider.models import Entity
from .serializers import (
    RefSerializer,
    ClientDataVerificationSerializer,
    LicenseSerializer,
    OTPGeneratorSerializer,
    OTPSerializer,
    Imageb64Serializer as ImageSerializer,
    IDBackExtractSerializer, FaceAntiSpoofingSerializer)
from .utils import _grab_image, extract_text_from_back_id, extract_text_from_face_id, \
    check_front_face_id_and_read, check_back_face_id_and_read


class CheckLicenseAPIView(PermissionAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = LicenseSerializer
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = LicenseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        entity = Entity.objects.select_related('license').get(ref=serializer.validated_data['issuer'])
        valid = entity.license.use_license()
        if valid:
            client = Client.objects.create(entity=entity, is_valid=True)
            return JsonResponse({
                'message': 'License is active',
                'code': '0001',

                'license_id': str(entity.license.license_id),
                'ref': client.ref

            }, status=200)
        return JsonResponse({
            'message': 'License is not active',
            'code': '0002'
        }, status=200)


class REKYClientAPIView(JWTAuthAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = RefSerializer

    def post(self, request, *args, **kwargs):
        """
        {
           'national_id': <string> M "National ID  ex: 29121xxxxxxxxxxxx",
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user
        client.national_id = data['national_id']
        # TODO: Checks of bundle id or app id
        # TODO: communicate
        mobile_nos = ['01273551124', '01022521187']
        client.available_mobile_no = mobile_nos
        client.save()
        return JsonResponse(
            {
                'message': 'client is created',

                'mobile_nos': mobile_nos,

                'code': '0003'
            }, status=201)


class OTPGeneratorAPIView(JWTAuthAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = OTPGeneratorSerializer

    def post(self, request, *args, **kwargs):
        """
        {
           'national_id': <string> M "National ID  ex: 29121xxxxxxxxxxxx",
           'mobile_no': <string> M "Mobile No. ex: 012xxxxxxx4"
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user
        if not data['mobile_no'] in client.available_mobile_no:
            return JsonResponse({
                'message': 'Mobile number is invalid',
                'code': '1000'
            }, status=200)
        client.mobile_no = data['mobile_no']
        if data['resend']:
            if client.no_of_sendings > 6:
                return JsonResponse({}, status=404)
        otp = client.generate_otp()
        client.save()
        client.send_sms()
        return JsonResponse(
            {'message': 'Otp is sent to {0}'.format(data['mobile_no']),

             'otp': otp,
             'code': '0004'
             }, status=200)


class OTPCheckAPIView(JWTAuthAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = OTPSerializer

    def post(self, request, *args, **kwargs):
        """
        {
           'otp': <string> M "OTP sent at ref generator  ex: 29121xxxxxxxxxxxx",
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user

        if client.check_otp(data['otp']):
            return JsonResponse({'message': 'This client is verified', 'code': '0005'}, status=201)

        else:
            return JsonResponse({'message': 'This client is not verified', 'code': '1000'}, status=200)


class FaceAntiSpoofing(JWTAuthAPIMixin, APIView):
    """
    API for verifing that client is real man and processing face image
    """
    # parser_classes = (parsers.MultiPartParser, parsers.FormParser)
    serializer_class = ImageSerializer
    parser_classes = (parsers.JSONParser,)

    def post(self, request, *args, **kwargs):
        """
        {
            :image: <multipart> M  "Image of the face"
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user
        client.face_img.save(name='{0}_{1}'.format('profile', data['image'].name), content=data['image'])
        return JsonResponse({'message': 'Face is saved correctly', 'code': '0006'}, status=200)


class IDFrontANDFaceVerifyAPIView(JWTAuthAPIMixin, APIView):
    # parser_classes = (parsers.MultiPartParser, parsers.FormParser)
    serializer_class = ImageSerializer
    parser_classes = (parsers.JSONParser,)

    def post(self, request, *args, **kwargs):
        """
        {
           'image': <multipart> M "Image of the id",
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user
        id_image = data['image']
        # id_image = _grab_image(stream=id_image)
        client.id_front_img.save(name='{0}_{1}'.format('id_front', id_image.name), content=data['image'])
        is_id_front, client_data, reason = check_front_face_id_and_read(client)
        print("REASON__" + str(reason))
        if is_id_front:
            client_data["name"] = client_data['first_name'] + " " + client_data["last_name"]
            client.full_name = client_data["name"]

            client.address = client_data['address']
            return JsonResponse(client_data, status=200)

        else:
            client.id_front_img.delete()
            if reason == 0:
                return JsonResponse({'message': 'Faces are not identical', 'code': '3001'}, status=200)
            elif reason == 1:
                return JsonResponse({'message': 'Image is not an front face of id', 'code': '3002'}, status=200)
            else:
                return JsonResponse({'message': 'Image is not an clear enough', 'code': '1002'}, status=200)


class IDBackCheckAPIView(JWTAuthAPIMixin, APIView):
    # parser_classes = (parsers.MultiPartParser, parsers.JSONParser)
    serializer_class = ImageSerializer
    parser_classes = (parsers.JSONParser,)

    def post(self, request, *args, **kwargs):
        """
        {
           'image': <multipart> M "Image of the id",
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        data = self.validate_data(request)
        client = request.user
        # back_image = _grab_image(stream=data['image'])
        client.id_back_img.save(name='{0}_{1}'.format('id_back', data['image'].name), content=data['image'])
        flag, client_data, reason = check_back_face_id_and_read(client)
        if flag:
            client.id_back_img.save(name='{0}_{1}'.format('id_back', data['image'].name), content=data['image'])
            # search for national id if found delete and move to log table and create the new one
            client_data.update({'message': 'Id is right', 'code': '0008'})
            return JsonResponse(client_data, status=200)
        client.id_back_img.delete()
        if reason == 1:
            return JsonResponse({'message': 'It is not back face for ID', 'code': '3001'}, status=200)
        elif reason == 2:
            return JsonResponse({'message': 'Image is not clear enough, please Try again', 'code': '3002'}, status=200)


class IDBackTextExtractorAPIView(JWTAuthAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = IDBackExtractSerializer

    def post(self, request, *args, **kwargs):
        """
        {
           'extract_flag': <bool> M "true / false",
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """

        data = self.validate_data(request)
        if data['extract_flag']:
            client = request.user
            back_text = extract_text_from_back_id(client.id_back_img)
            back_text.update({'message': 'Client is verified', 'code': '0009'})
            return JsonResponse(back_text, status=200)
        return JsonResponse({'message': 'Client is not verified', 'code': '1001'}, status=200)


class ClientDataVerificationAPIView(JWTAuthAPIMixin, APIView):
    parser_classes = (parsers.JSONParser,)
    serializer_class = ClientDataVerificationSerializer

    def post(self, request, *args, **kwargs):
        """
        {
           'national_id': <string> M "National ID  ex: 29121xxxxxxxxxxxx",
           'mobile_no': <string> M "Mobile No. ex: 012xxxxxxx4"
        }
        :param request: request of the user
        :param args: arguments
        :param kwargs: keyword argyments
        :return: JsonResponse with the reference generated
        """
        serializer = ClientDataVerificationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        client = request.user
        face_text = data['face_text']
        back_text = data['back_text']
        is_verified = client.verify(face_text, back_text)
        if is_verified:
            client.is_valid = True
            client.save()
            return JsonResponse({
                'message': 'Client is KYC-ed',
                'face': face_text,
                'back': back_text,
                'ref': client.ref,
                'code': '0010'}, status=201)
        return JsonResponse({'message': 'Client is not KYC-ed yet', 'code': '1002'}, status=200)


check_license = CheckLicenseAPIView.as_view()
reky_client = REKYClientAPIView.as_view()
otp_generator = OTPGeneratorAPIView.as_view()
otp_check = OTPCheckAPIView.as_view()
face_anti_spoofing = FaceAntiSpoofing.as_view()
id_front_face_verify = IDFrontANDFaceVerifyAPIView.as_view()
id_back_check = IDBackCheckAPIView.as_view()
id_back_extract = IDBackTextExtractorAPIView.as_view()
client_data_verify = ClientDataVerificationAPIView.as_view()
