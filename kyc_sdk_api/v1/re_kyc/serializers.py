from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from kyc_sdk_api.v1.mixins import StatusCodeSerializerMixin
from users.client.models import Client
from drf_extra_fields.fields import Base64ImageField


class Serializer(StatusCodeSerializerMixin, serializers.Serializer):
    pass


class LicenseSerializer(Serializer):
    issuer = serializers.UUIDField()


class RefSerializer(Serializer):
    national_id = serializers.CharField(max_length=14, min_length=14)
    bundle_id = serializers.CharField(required=False)
    app_id = serializers.CharField(required=False)
    device_id = serializers.CharField(required=True)

    def validate(self, attrs):
        dev_id = attrs.get('bundle_id', None) or attrs.get('app_id', None)
        if dev_id:
            return attrs
        raise ValidationError('One of these fields bundle_id or device_id is required')


class OTPGeneratorSerializer(Serializer):
    mobile_no = serializers.CharField()
    resend = serializers.BooleanField(default=False)

    def validate_mobile_no(self, mobile_no):
        if mobile_no:
            import re
            r = re.compile('(201|01)[0-2|5]\d{7}')
            if r.match(mobile_no):
                return mobile_no
            else:
                raise ValidationError("Mobile Number is not valid")
        else:
            return mobile_no


class OTPSerializer(Serializer):
    otp = serializers.CharField()


class ImageSerializer(Serializer):
    image = serializers.ImageField(allow_null=False, allow_empty_file=False)


class Imageb64Serializer(Serializer):
    image = Base64ImageField(required=True)


class FaceAntiSpoofingSerializer(StatusCodeSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('face_img',)


class IDBackExtractSerializer(Serializer):
    extract_flag = serializers.BooleanField(default=True)


class FaceIDTextSerializer(Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    district = serializers.CharField()
    city = serializers.CharField()
    national_id = serializers.CharField(max_length=14, min_length=14)


class BackIDTextSerializer(Serializer):
    marital_status = serializers.ChoiceField(choices=['Married', 'Widower', 'Divorced'])
    gender = serializers.ChoiceField(choices=['Male', 'Female'])
    religion = serializers.ChoiceField(choices=['Muslim', 'Christian', 'Jew'])
    valid_till = serializers.DateField(input_formats=['%d/%m/%Y'])


class ClientDataVerificationSerializer(Serializer):
    face_text = FaceIDTextSerializer(required=True)
    back_text = BackIDTextSerializer(required=True)

