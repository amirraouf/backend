from django.conf.urls import url

from kyc_sdk_api.v1.re_kyc.views import (
    check_license,
    reky_client,
    otp_generator,
    otp_check,
    face_anti_spoofing,
    id_front_face_verify,
    id_back_check,
    id_back_extract,
    client_data_verify,
)

urls = [
    url(r'^license/$', check_license),
    url(r'^reky_client/$', reky_client),
    url(r'^otp_generator/$', otp_generator),
    url(r'^otp_check/$', otp_check),
    url(r'^face_anti_spoofing/$', face_anti_spoofing),
    url(r'^id_front_face_verify/$', id_front_face_verify),
    url(r'^id_back_check/$', id_back_check),
    url(r'^id_back_extract/$', id_back_extract),
    url(r'^client_data_verify/$', client_data_verify),
]
