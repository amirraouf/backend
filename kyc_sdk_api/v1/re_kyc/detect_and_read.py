import re
from difflib import get_close_matches
from tempfile import NamedTemporaryFile

import operator
import numpy as np
import cv2
import os
import pytesseract
import face_recognition
from PIL import Image, ImageDraw, ImageEnhance


def ab_with_check(text):
    for ch in '0{}[123456789!@#$^&*()-_=\|\'.,";:/%/«؟':
        if ch in text:
            text = text.replace(ch, "")
    return text



def find_if_close(cnt1, cnt2, min_dist=40):
    row1, row2 = cnt1.shape[0], cnt2.shape[0]
    for i in range(row1):
        for j in range(row2):
            dist = np.linalg.norm(cnt1[i] - cnt2[j])
            if abs(dist) < min_dist:
                return True
            elif i == row1 - 1 and j == row2 - 1:
                return False


def merge_cnts(cnt1, cnt2):
    x1, y1, w1, h1 = cv2.boundingRect(cnt1)
    x2, y2, w2, h2 = cv2.boundingRect(cnt2)
    if x1 < x2:
        x = x1
        w = x1 + w1 if x2 + w2 < x1 + w1 else x2 + w2
    else:
        x = x2
        w = x1 + w1 if x2 + w2 < x1 + w1 else x2 + w2
    if y1 < y2:
        y = y1
        h = y2 + h2 if y2 + h2 > y1 + h1 else y1 + h1
    else:
        y = y2
        h = y2 + h2 if y2 + h2 > y1 + h1 else y1 + h1
    return x, y, w, h


def mergeOverlappingBoxes(inputBoxes, image, biggest=False):
    mask = np.zeros((image.shape[0], image.shape[1],1), dtype=np.uint8)  # Mask of original image
    # To expand rectangles, i.e. increase sensitivity to nearby rectangles. Doesn't have to be (10,10)--can be anything
    outputBoxes = []
    for i in range(len(inputBoxes)):
        box = inputBoxes[i]  # + scaleFactor;
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        cv2.rectangle(mask, (x, y), (w - 1, h - 1), (255, 255, 255), cv2.FILLED)  # Draw filled bounding boxes on mask
    # Find contours in mask
    # If bounding boxes overlap, they will be joined by this function call
    _, bws = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
    img, contours, hierarchy = cv2.findContours(bws, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE, offset=(0,0))
    cnts = sorted(contours, key=cv2.contourArea, reverse=True)
    if biggest:
        return [cv2.boundingRect(cnts)]
    # compute the rotated bounding box of the largest contour
    print(len(cnts))
    for c in cnts:
        outputBoxes.append(cv2.boundingRect(c))
    return outputBoxes


class DetectANDReadID:
    def __init__(self, patterns_paths, image_path, img=None, detect=True):
        self.patterns_paths = patterns_paths
        print(image_path)
        if image_path:
            self.image_path = image_path
            self.image = cv2.imread(image_path, 0)
        elif img is not None:
            self.image = img
            d = os.getpid()
            image_path = "/tmp/{}.png".format(str(d))
            self.image_path = image_path
            cv2.imwrite(filename=image_path, img=self.image)

        self.sift = cv2.xfeatures2d.SIFT_create()
        self.good = []
        self.counter = 0
        # self.enhance()
        if detect:
            self.can_start = self.start_initially()
            self.rotate()
        else:
            pass

    # def __del__(self):
    #     del self.image

    def start_initially(self):
        success_counter = 0
        for item in self.patterns_paths:
            if self._is_pattern_found(item):
                success_counter += 1
        return success_counter == len(self.patterns_paths)

    def start(self):
        return self.can_start

    # search for pattern in image using sift algorithm
    def _is_pattern_found(self, pattern, purpose='compare'):
        pattern_image = cv2.imread(pattern, 0)
        kp1, des1 = self.sift.detectAndCompute(self.image, None)
        kp2, des2 = self.sift.detectAndCompute(pattern_image, None)
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1.astype(np.float32, copy=False), des2.astype(np.float32, copy=False), k=2)
        counter = 0
        self.counter = 0
        for i, (m, n) in enumerate(matches):
            if m.distance < 0.7 * n.distance:
                self.good.append(m)
                counter += 1
        if counter > self.counter:
            self.counter = counter
            self.better_pattern = pattern_image
            self.kp1 = kp1
            self.kp2 = kp2
            self.des1 = des1
            self.des2 = des2
        else:
            print('NOT COUNT')
            pass

        if counter >= 25:
            if purpose == 'compare' or purpose == 'rotate':
                return True
            else:
                src_pts = np.float32([kp1[m.queryIdx].pt for m in self.good]).reshape(-1, 1, 2)
                return src_pts
        else:
            return False

    def rotate(self):
        if self.can_start:
            self.enhance()
            h, w = self.image.shape
            src_pts = np.float32([self.kp1[m.queryIdx].pt for m in self.good]).reshape(-1, 1, 2)
            dst_pts = np.float32([self.kp2[m.trainIdx].pt for m in self.good]).reshape(-1, 1, 2)
            H, _ = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
            im_dst = cv2.warpPerspective(self.image, H, (w, h), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
            # name = self.image_path.split(".")
            # name = name[0] + "_rotated" + "." + name[1]
            ret, thresh = cv2.threshold(im_dst, 1, 255, 0)
            im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            c = sorted(contours, key=cv2.contourArea, reverse=True)[0]

            [intX, intY, intWidth, intHeight] = cv2.boundingRect(c)
            cropped = im_dst[intY: intY + intHeight, intX: intX + intWidth]
            # cv2.imwrite(name, cropped)
            self.image = cropped
            # self.rotated_image_path = name
            return True
        else:
            return False

    def recognize_face(self, q_image):
        """

        :param image: image of the face in the id
        :param q_image: image of the face from the cam
        :return:
        """
        face_img = face_recognition.load_image_file(q_image)
        id_img = face_recognition.load_image_file(self.image_path)
        q_face_encoding = face_recognition.face_encodings(id_img)[0]

        face_locations = face_recognition.face_locations(face_img)
        face_encodings = face_recognition.face_encodings(face_img, face_locations)

        # Loop through each face in this image
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            match = face_recognition.compare_faces([q_face_encoding], face_encoding)

            result = False
            if match[0]:
                result = True
            return result

    @staticmethod
    def detect_detail_text(base_img, biggest=True):
        rgb = cv2.pyrDown(base_img)
        small = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)

        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
        grad = cv2.morphologyEx(small, cv2.MORPH_GRADIENT, kernel)

        _, bw = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 1))
        connected = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)
        # using RETR_EXTERNAL instead of RETR_CCOMP
        img, contours, hierarchy = cv2.findContours(connected, cv2.RETR_EXTERNAL, 2)
        mask = np.zeros(bw.shape, dtype=np.uint8)
        new_countours = []
        for idx in range(len(contours)):
            x, y, w, h = cv2.boundingRect(contours[idx])
            mask[y:y + h, x:x + w] = 0
            cv2.drawContours(mask, contours, idx, (255, 255, 255), -1)
            r = float(cv2.countNonZero(mask[y:y + h, x:x + w])) / (w * h)
            if r > 0.45 and w > 15 and h > 15:
                new_countours.append(contours[idx])

        ###############

        LENGTH = len(new_countours)
        unified = []
        min_dist = round(base_img.shape[1] * 0.025) + 5
        for i, cnt1 in enumerate(new_countours):
            x = i
            if i != LENGTH - 1:
                for j, cnt2 in enumerate(new_countours[i + 1:]):
                    x = x + 1
                    dist = find_if_close(cnt1, cnt2, min_dist)
                    if dist == True:
                        val = merge_cnts(cnt1, cnt2)
                        unified.append(val)
        copy = rgb.copy()

        unified2 = mergeOverlappingBoxes(unified, copy, biggest=biggest)
        for ix, rect in enumerate(unified2):
            print(rect)
            x = rect[0] * 2
            y = rect[1] * 2
            w = rect[2] * 2
            h = rect[3] * 2

            cropped = base_img[y: y + h, x: x + w]
            return cropped

    def detect_barcode(self):
        # gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        gradX = cv2.Sobel(self.image, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
        gradY = cv2.Sobel(self.image, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=-1)

        # subtract the y-gradient from the x-gradient
        # this subtraction we are left with regions of the image
        # that have high horizontal gradients and low vertical gradients
        gradient = cv2.subtract(gradX, gradY)
        gradient = cv2.convertScaleAbs(gradient)

        blurred = cv2.blur(gradient, (5, 5))
        (_, thresh) = cv2.threshold(blurred, 225, 255, cv2.THRESH_BINARY)

        # construct a closing kernel and apply it to the thresholded image
        # to close the gaps between barcode stripes
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 40))
        closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

        # now we have small blobs in the image that are not part of the actual barcode,
        # but may interfere with our contour detection
        # perform a series of erosions and dilations
        closed = cv2.erode(closed, None, iterations=4)
        closed = cv2.dilate(closed, None, iterations=4)

        # find the contours in the thresholded image, then sort the contours
        # by their area, keeping only the largest one
        img, cnts, hierarchy = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
                                                cv2.CHAIN_APPROX_SIMPLE)
        c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

        # compute the rotated bounding box of the largest contour

        # draw a bounding box arounded the detected barcode and display the
        # image
        [intX, intY, intWidth, intHeight] = cv2.boundingRect(c)
        self.image = self.image[: intY , intX: intX + intWidth]
        x, y = self.image.shape[:2]
        image_cropped = self.image[:, y // 4:round((3.4) * y) // 4]
        x, y = image_cropped.shape[:2]
        self.national_img = image_cropped[: y // 6, round(0.35*x):]
        self.data_img = image_cropped[8*y // 60:  y // 2, :]
        self.valid_img = image_cropped[y // 2:, :]

    def crop_faceId(self):
        face_locations = face_recognition.face_locations(self.image)
        top, right, bottom, left = face_locations[0]
        imgBlurred = cv2.GaussianBlur(self.image, (3, 3), 0)  # blur
        # imgBlurred = cv2.medianBlur(self.image, 3)

        # filter image from grayscale to black and white
        imgThresh = cv2.adaptiveThreshold(imgBlurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,13)

        text_im = imgThresh[top:round(bottom * 1.8), right:]
        y = text_im.shape[1]
        x = text_im.shape[0]
        national_id_start_left = round(1.5 * y / 6.5)
        national_id_start_top = round(2.9 * x / 4.3)
        national_id_start_bottom = round(4 * x / 4.3)
        national_id_start_right = round(7.5 * y / 8)
        national = text_im[national_id_start_top:national_id_start_bottom, national_id_start_left:national_id_start_right]
        detail = text_im[:round(bottom * 0.85), :]
        return detail, national

    @staticmethod
    def read_national(imgThresh):
        imgThresh = cv2.pyrUp(imgThresh)
        imgThresh = cv2.adaptiveThreshold(imgThresh,  # input image
                                          255,  # make pixels that pass the threshold full white
                                          cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                          # use gaussian rather than mean, seems to give better results
                                          cv2.THRESH_BINARY_INV,
                                          # invert so foreground will be white, background will be black
                                          15,  # size of a pixel neighborhood used to calculate threshold value
                                          2)  # constant subtracted from the mean or weighted mean
        MIN_CONTOUR_AREA = imgThresh.size//1000

        RESIZED_IMAGE_WIDTH = 20
        RESIZED_IMAGE_HEIGHT = 30

        class ContourWithData():
            npaContour = None  # contour
            boundingRect = None  # bounding rect for contour
            intRectX = 0  # bounding rect top left corner x location
            intRectY = 0  # bounding rect top left corner y location
            intRectWidth = 0  # bounding rect width
            intRectHeight = 0  # bounding rect height
            fltArea = 0.0  # area of contour

            def calculateRectTopLeftPointAndWidthAndHeight(self):  # calculate bounding rect info
                [intX, intY, intWidth, intHeight] = self.boundingRect
                self.intRectX = intX
                self.intRectY = intY
                self.intRectWidth = intWidth
                self.intRectHeight = intHeight

            def checkIfContourIsValid(self):  # this is oversimplified, for a production grade program
                if self.fltArea < MIN_CONTOUR_AREA: return False  # much better validity checking would be necessary
                return True

        allContoursWithData = []  # declare empty lists,
        validContoursWithData = []  # we will fill these shortly
        from django.conf import settings
        root = settings.APPS_DIR
        classifications_text = "{}{}".format(root, "/patterns/classifications.txt").replace("/kyc_backend/kyc_backend/", "/kyc_backend/")
        flattened_text = "{}{}".format(root, "/patterns/flattened_images.txt").replace("/kyc_backend/kyc_backend/", "/kyc_backend/")
        try:
            npaClassifications = np.loadtxt(classifications_text, np.float32)  # read in training classifications
        except:
            print("error, unable to open classifications.txt, exiting program\n")
            return
        # end try

        try:
            npaFlattenedImages = np.loadtxt(flattened_text, np.float32)  # read in training images
        except:
            print("error, unable to open flattened_images.txt, exiting program\n")
            return
        # end try

        npaClassifications = npaClassifications.reshape(
            (npaClassifications.size, 1))  # reshape numpy array to 1d, necessary to pass to call to train

        kNearest = cv2.ml.KNearest_create()  # instantiate KNN object

        kNearest.train(npaFlattenedImages, cv2.ml.ROW_SAMPLE, npaClassifications)

        if imgThresh is None:  # if image was not read successfully
            print("error: image not read from file \n\n")  # print error message to std out
            return  # and exit function (which exits program)
        # end if

        kernel = np.ones((2, 2), np.uint8)
        dilate = cv2.dilate(imgThresh, kernel, iterations=1)
        kernel = np.ones((3, 2), np.uint8)
        erosion = cv2.erode(dilate, kernel, iterations=1)
        kernel = np.ones((1, 1), np.uint8)
        closing = cv2.morphologyEx(erosion, cv2.MORPH_CLOSE, kernel)
        kernel = np.ones((2, 2), np.uint8)
        opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel)
        imgContours, npaContours, npaHierarchy = cv2.findContours(opening,
                                                                  cv2.RETR_EXTERNAL,
                                                                  cv2.CHAIN_APPROX_SIMPLE)
        print(len(npaContours))
        for npaContour in npaContours:
            contourWithData = ContourWithData()
            contourWithData.npaContour = npaContour
            contourWithData.boundingRect = cv2.boundingRect(contourWithData.npaContour)
            contourWithData.calculateRectTopLeftPointAndWidthAndHeight()
            contourWithData.fltArea = cv2.contourArea(contourWithData.npaContour)
            allContoursWithData.append(
                contourWithData)

        for contourWithData in allContoursWithData:
            if contourWithData.checkIfContourIsValid():
                validContoursWithData.append(contourWithData)

        validContoursWithData.sort(key=operator.attrgetter("intRectX"))

        strFinalString = ""

        for contourWithData in validContoursWithData:
            imgROI = imgThresh[contourWithData.intRectY: contourWithData.intRectY + contourWithData.intRectHeight,

                     contourWithData.intRectX: contourWithData.intRectX + contourWithData.intRectWidth]

            imgROIResized = cv2.resize(imgROI, (RESIZED_IMAGE_WIDTH,
                                                RESIZED_IMAGE_HEIGHT))

            npaROIResized = imgROIResized.reshape((1, RESIZED_IMAGE_WIDTH * RESIZED_IMAGE_HEIGHT))

            npaROIResized = np.float32(
                npaROIResized)

            retval, npaResults, neigh_resp, dists = kNearest.findNearest(npaROIResized,
                                                                         k=1)

            strCurrentChar = str(chr(int(npaResults[0][0])))

            strFinalString = strFinalString + strCurrentChar
        print(strFinalString)
        return strFinalString

    @staticmethod
    def read_front_detail(imgThresh):
        kernel = np.ones((1, 1), np.uint8)
        im_bw = cv2.dilate(imgThresh, kernel, iterations=10)

        # cv2.putText(im_bw, " a 0",
        #             (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        filename = "{}.png".format(os.getpid())
        cv2.imwrite(filename, im_bw)
        # load the image as a PIL/Pillow image, apply OCR, and then delete
        # the temporary file
        text = pytesseract.image_to_string(Image.open(filename), lang='ara', config='--psm 6')
        print(text)
        # config="--tessdata-dir ../facerec/tessdata")

        text = ab_with_check(text)
        text = re.sub(r"[\n]+", repl="\n", string=text)
        data = text.split("\n")
        os.remove(filename)
        if len(data) < 4:
            return None
        data_dict = {
            'first_name': re.sub(r'[^\w\s-]', '', data[0]).strip(),
            'last_name': re.sub(r'[^\w\s-]', '', data[1]).strip(),
            'address': re.sub(r'[^\w\s-]', '', data[2]).strip(),
            'city': re.sub(r'[^\w\s-]', '', data[3]).strip(),
        }
        return data_dict

    def crop_backId(self):
        pass

    def read(self, face='front'):
        if face == 'front':
            detail, national = self.crop_faceId()
            national_txt = self.read_national(national)
            # if national_txt and len(national_txt)==14:
            # detail = self.detect_detail_text(detail)
            detail_txt = self.read_front_detail(detail)
            if detail_txt:
                detail_txt.update({'national_id': national_txt})
                detail_txt['message'] = 'Data is extracted successfully'
                detail_txt['code'] = '0007'
                return detail_txt
            return None
        elif face == 'back':
            self.detect_barcode()
            national_txt = self.read_national(self.national_img)
            valid_text = self.read_national(self.valid_img)

            self.crop_data_img()
            gender = get_close_matches(self.read_back_detail(self.gender), ['ذكر', 'أنثى'], n=1, cutoff=0.3)
            if gender:
                gender = gender[0]
            else:
                gender = ''

            if gender == 'ذكر':
                statuses = ['أعزب', 'مطلق', 'متزوج']
            else:
                statuses = ['أنسه', 'مطلقة', 'متزوجة']

            religion = get_close_matches(self.read_back_detail(self.religion), ['مسيحي', 'مسلم'], n=1, cutoff=0.6)
            status = get_close_matches(self.read_back_detail(self.status), statuses, n=1,
                                       cutoff=0.3)
            if religion:
                religion = religion[0]
            else:
                religion = ''
            if status:
                status = status[0]
            else:
                status = ''
            data_txt = {
                'national_id': national_txt,
                'valid': valid_text,
                'work': self.read_back_detail(self.work),
                'gender': gender,
                'religion': religion,
                'status': status
            }
            return data_txt

    @staticmethod
    def read_back_detail(imgThresh):
        kernel = np.ones((1, 1), np.uint8)
        im_bw = cv2.dilate(imgThresh, kernel, iterations=10)

        filename = "{}.png".format(os.getpid())
        cv2.imwrite(filename, im_bw)
        text = pytesseract.image_to_string(Image.open(filename), lang='ara', config='--psm 6')
        print(text)
        # config="--tessdata-dir ../facerec/tessdata")

        text = ab_with_check(text)
        text = re.sub(r"[\n]+", repl="\n", string=text)

        os.remove(filename)
        return text

    def crop_data_img(self):
        y, x = self.data_img.shape[:2]
        print(x, y)
        imgBlurred = cv2.GaussianBlur(self.data_img, (3, 3), 0)  # blur
        # imgBlurred = cv2.medianBlur(self.image, 3)

        # filter image from grayscale to black and white
        imgThresh = cv2.adaptiveThreshold(imgBlurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 21, 11)

        self.work = imgThresh[:round(0.56 * y),:]
        self.gender = imgThresh[round(0.56 * y):, round(x * 0.8):]
        self.religion = imgThresh[round(0.56 * y):, round(x * 0.45):round(x * 0.7)]
        self.status = imgThresh[round(0.56 * y):, :round(x * 0.3)]

    def _read_back(self):
        self.data_img[self.data_img < 100] = 0
        self.data_img[self.data_img > 160] = 255
        converted_img = cv2.cvtColor(self.data_img, cv2.COLOR_GRAY2BGR)
        dst = cv2.fastNlMeansDenoisingColored(converted_img, None, 10, 10, 7, 21) ## IMPORTANT
        gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
        # gray[gray > 225] = 255
        # gray = cv2.medianBlur(gray, 3)
        ###################

        kernel = np.ones((1, 1), np.uint8)
        th3 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 21, 13)  # bk

        #########============+#####
        blur = cv2.GaussianBlur(th3, (5, 5), 0)

        ret3, im_bw = cv2.threshold(blur, 100, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        ################
        im_bw = cv2.bitwise_not(im_bw)
        img = cv2.dilate(im_bw, kernel, iterations=10)
        im_bw = cv2.bitwise_not(img)

        filename = "{}.png".format(os.getpid())
        cv2.imwrite(filename, im_bw)
        cv2.waitKey(1000)
        # load the image as a PIL/Pillow image, apply OCR, and then delete
        # the temporary file
        # tool = pyocr.get_available_tools()[0]
        text = pytesseract.image_to_string(Image.open(filename), lang='ara',
                                          config='-psm 5')  # , builder=pyocr.tesseract.DigitBuilder())
        print(text)
        text = ab_with_check(text)
        text = re.sub(r"[\n]+", repl="\n", string=text)
        os.remove(filename)
        data = text.split('\n')

        info = data[-1].split(' ')
        return data, info

    def _read_back1(self):
        """

        :return:
        """
        # converted_img = cv2.cvtColor(self.data_img, cv2.COLOR_GRAY2BGR)
        ret3, im_bw = cv2.threshold(self.data_img, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        dist = cv2.distanceTransform(im_bw, cv2.DIST_L2, cv2.DIST_MASK_PRECISE)

        SWTHRESH = 1.0
        _, dibw = cv2.threshold(dist, SWTHRESH / 2, 255.0, cv2.THRESH_BINARY)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
        morph = cv2.morphologyEx(dibw, cv2.MORPH_OPEN, kernel)
        try:
            dibw = cv2.cvtColor(dibw, cv2.CV_8U)
        except:
            pass
        try:
            binary = cv2.cvtColor(dibw, cv2.CV_8U)
        except:
            binary= dibw
        cv2.waitKey(5000)
        cv2.destroyAllWindows()
        exit()
        cont = cv2.cvtColor(morph, cv2.CV_8U)

        hthresh = self.data_img.shape[1] * 0.5
        image, contours, hierarchy = cv2.findContours(cont,cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE) #, (0,0))
        digits = []
        for i in range(len(hierarchy)):
            [intX, intY, intWidth, intHeight] = cv2.boundingRect(contours[i])
            if intHeight > hthresh:
                digits.append(contours[i][0])
                digits.append(contours[i][-1])
                cv2.rectangle(binary, (intX,intY), (intX+intWidth-1, intY+intHeight-1), (0,0,255), 2)
        digitshull = cv2.convexHull(digits)
        digitsmask = np.zeros((self.data_img.shape[0], self.data_img.shape[1], 0))
        cv2.drawContours(digitsmask, digitshull, 0, (255,255,255), -1)
        digitsmask = cv2.morphologyEx(digitsmask, cv2.MORPH_DILATE, kernel)

        cleaned = dibw * (digitsmask.astype(dibw.dtype))
        cv2.waitKey(0)

    def detect(self):
        """
        std::vector<cv::Rect> detectLetters(cv::Mat img)
        {
            std::vector<cv::Rect> boundRect;
            cv::Mat img_gray, img_sobel, img_threshold, element;
            cvtColor(img, img_gray, CV_BGR2GRAY);
            cv::Sobel(img_gray, img_sobel, CV_8U, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
            cv::threshold(img_sobel, img_threshold, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
            element = getStructuringElement(cv::MORPH_RECT, cv::Size(17, 3) );
            cv::morphologyEx(img_threshold, img_threshold, CV_MOP_CLOSE, element); //Does the trick
            std::vector< std::vector< cv::Point> > contours;
            cv::findContours(img_threshold, contours, 0, 1);
            std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
            for( int i = 0; i < contours.size(); i++ )
                if (contours[i].size()>100)
                {
                    cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 3, true );
                    cv::Rect appRect( boundingRect( cv::Mat(contours_poly[i]) ));
                    if (appRect.width>appRect.height)
                        boundRect.push_back(appRect);
                }
            return boundRect;
        }
        :return:
        """
        pass

    @staticmethod
    def detect_text(base_img, min_dist=40):
        rgb = cv2.pyrDown(base_img)
        try:
            small = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
        except:
            small = rgb

        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
        grad = cv2.morphologyEx(small, cv2.MORPH_GRADIENT, kernel)

        _, bw = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 1))
        connected = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)
        # using RETR_EXTERNAL instead of RETR_CCOMP
        img, contours, hierarchy = cv2.findContours(connected.copy(), cv2.RETR_EXTERNAL, 2)
        mask = np.zeros(bw.shape, dtype=np.uint8)
        highest_h = 0
        new_countours = []
        for idx in range(len(contours)):
            x, y, w, h = cv2.boundingRect(contours[idx])
            mask[y:y + h, x:x + w] = 0
            cv2.drawContours(mask, contours, idx, (255, 255, 255), -1)
            r = float(cv2.countNonZero(mask[y:y + h, x:x + w])) / (w * h)
            if r > 0.45 and w > 15 and h > 15:
                new_countours.append(contours[idx])

        ###############

        LENGTH = len(new_countours)
        status = np.zeros((LENGTH, 1))
        unified = []
        min_dist = round(base_img.shape[1] * 0.025) + 5
        for i, cnt1 in enumerate(new_countours):
            x = i
            if i != LENGTH - 1:
                for j, cnt2 in enumerate(new_countours[i + 1:]):
                    x = x + 1
                    dist = find_if_close(cnt1, cnt2, min_dist)
                    if dist == True:
                        val = merge_cnts(cnt1, cnt2)
                        unified.append(val)
        copy = rgb.copy()
        unified2 = mergeOverlappingBoxes(unified, copy)
        if len(unified2):
            for ix, rect in enumerate(unified2):
                x = rect[0] * 2
                y = rect[1] * 2
                w = rect[2] * 2
                h = rect[3] * 2
                cropped = base_img[y: y + h, x: x + w]
            return cropped
        else:
            return base_img

    def enhance(self):
        image = Image.fromarray(self.image)
        enhancer = ImageEnhance.Color(image)
        enhanced = enhancer.enhance(1.0)
        enhancer = ImageEnhance.Brightness(enhanced)
        enhanced = enhancer.enhance(1.2)
        enhancer = ImageEnhance.Sharpness(enhanced)

        enhanced = enhancer.enhance(2.0)
        enhancer = ImageEnhance.Sharpness(enhanced)
        enhanced = enhancer.enhance(1.0)

        image.paste(enhanced)
        name = self.image_path.split(".")
        name = name[0] + "_rotated" + "." + name[1]
        image.save(name)
        self.image = cv2.imread(name, 0)
