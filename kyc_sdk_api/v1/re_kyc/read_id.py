import re

from PIL import Image
import pytesseract
import cv2
import os
import numpy as np
import face_recognition
from django.conf import settings

from kyc_sdk_api.v1.re_kyc.detect_and_read import DetectANDReadID


def crop_face(path):
    id_image = face_recognition.load_image_file(path)

    face_locations = face_recognition.face_locations(id_image)
    top, right, bottom, left = face_locations[0]
    paths = path.split(".")
    cv2.imwrite("{}_face.{}".format(paths[0],paths[1]), id_image[top:bottom, left:right])


def crop_id_except_face(id_image=None, path=None):
    if path:
        id_image = face_recognition.load_image_file(path)
    height, width = id_image.shape[:2]  # Get dimensions
    face_locations = face_recognition.face_locations(id_image)
    top, right, bottom, left = face_locations[0]
    return id_image[top:bottom*2, right:width]


def find_if_close(cnt1, cnt2, min_dist):
    row1, row2 = cnt1.shape[0], cnt2.shape[0]
    for i in range(row1):
        for j in range(row2):
            dist = np.linalg.norm(cnt1[i] - cnt2[j])
            if abs(dist) < 50:
                return True
            elif i == row1 - 1 and j == row2 - 1:
                return False


def merge_cnts(cnt1, cnt2):
    x1, y1, w1, h1 = cv2.boundingRect(cnt1)
    x2, y2, w2, h2 = cv2.boundingRect(cnt2)
    if x1 < x2:
        x = x1
        w = x1 + w1 if x2 + w2 < x1 + w1 else x2 + w2
    else:
        x = x2
        w = x1 + w1 if x2 + w2 < x1 + w1 else x2 + w2
    if y1 < y2:
        y = y1
        h = y2 + h2 if y2 + h2 > y1 + h1 else y1 + h1
    else:
        y = y2
        h = y2 + h2 if y2 + h2 > y1 + h1 else y1 + h1
    return x, y, w, h


def mergeOverlappingBoxes(inputBoxes, image):
    mask = np.zeros((image.shape[0], image.shape[1],1), dtype=np.uint8)  # Mask of original image
    # To expand rectangles, i.e. increase sensitivity to nearby rectangles. Doesn't have to be (10,10)--can be anything
    outputBoxes = []
    for i in range(len(inputBoxes)):
        box = inputBoxes[i]  # + scaleFactor;
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        cv2.rectangle(mask, (x, y), (w - 1, h - 1), (255, 255, 255), cv2.FILLED)  # Draw filled bounding boxes on mask
    # Find contours in mask
    # If bounding boxes overlap, they will be joined by this function call
    _, bws = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
    img, contours, hierarchy = cv2.findContours(bws, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE, offset=(0,0))
    c = sorted(contours, key=cv2.contourArea, reverse=True)[0]

    # compute the rotated bounding box of the largest contour
    # for j in range(len(c)):
    rect = cv2.minAreaRect(c)
    box = np.int0(cv2.boxPoints(rect))
    outputBoxes.append(box)
    return outputBoxes


def detect_text_face_place(path):
    base_img = cv2.imread(path)
    cropped = crop_id_except_face(base_img)
    rgb = cv2.pyrDown(cropped)
    small = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
    grad = cv2.morphologyEx(small, cv2.MORPH_GRADIENT, kernel)

    _, bw = cv2.threshold(grad, 0.0, 255.0, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 1))
    connected = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)
    # using RETR_EXTERNAL instead of RETR_CCOMP
    img, contours, hierarchy = cv2.findContours(connected.copy(), cv2.RETR_EXTERNAL, 2)
    mask = np.zeros(bw.shape, dtype=np.uint8)
    new_countours = []
    for idx in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[idx])
        mask[y:y + h, x:x + w] = 0
        cv2.drawContours(mask, contours, idx, (255, 255, 255), -1)
        r = float(cv2.countNonZero(mask[y:y + h, x:x + w])) / (w * h)
        if r > 0.45 and w > 15 and h > 15:
            new_countours.append(contours[idx])

    ###############

    LENGTH = len(new_countours)
    unified = []
    min_dist = round(base_img.shape[1] * 0.025) + 5
    for i, cnt1 in enumerate(new_countours):
        x = i
        if i != LENGTH - 1:
            for j, cnt2 in enumerate(new_countours[i + 1:]):
                x = x + 1
                dist = find_if_close(cnt1, cnt2, min_dist)
                if dist == True:
                    val = merge_cnts(cnt1, cnt2)
                    unified.append(val)

    unified2 = mergeOverlappingBoxes(unified, rgb)
    paths = path.split(".")
    for idx, rect in enumerate(unified2):
        x = rect[0][0]
        y = rect[0][1]
        w = rect[2][0]
        h = rect[2][1]
        try:
            cropped = base_img[h*2:y*2, w*2:x*2]
        except:
            cropped = base_img[h*2:y*2, x*2:w*2]
        cv2.imwrite("{}_text_{}.{}".format(paths[0],str(idx), paths[1]), cropped)
    return len(unified2)


def detect_barcode_and_crop(im, path):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    # compute the Scharr gradient magnitude representation of the images
    # in both the x and y direction
    gradX = cv2.Sobel(gray, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
    gradY = cv2.Sobel(gray, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=-1)

    # subtract the y-gradient from the x-gradient
    gradient = cv2.subtract(gradX, gradY)
    gradient = cv2.convertScaleAbs(gradient)

    blurred = cv2.blur(gradient, (5, 5))
    (_, thresh) = cv2.threshold(blurred, 225, 255, cv2.THRESH_BINARY)

    # construct a closing kernel and apply it to the thresholded image
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 40))
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    # perform a series of erosions and dilations
    closed = cv2.erode(closed, None, iterations=4)
    closed = cv2.dilate(closed, None, iterations=4)

    # find the contours in the thresholded image, then sort the contours
    # by their area, keeping only the largest one
    img, cnts, hierarchy = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
                                            cv2.CHAIN_APPROX_SIMPLE)
    c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

    # compute the rotated bounding box of the largest contour
    rect = cv2.minAreaRect(c)
    box = np.int0(cv2.boxPoints(rect))

    # draw a bounding box arounded the detected barcode and display the
    # image
    paths = path.split(".")
    x = box[0][0]
    # y = box[0][1]
    w = box[2][0]
    h = box[2][1]
    im_c = im.copy()
    height, width = im_c.shape[:2]
    try:
        cropped = im[0:h, x:w]
    except:
        cropped = im[0:h, w:x]
    new_path = "{}_text_back.{}".format(paths[0], paths[1])
    cv2.imwrite(new_path, cropped)
    return new_path, cropped


def read_face_id(path):
    root = settings.APPS_DIR
    patterns = ["{}{}".format(root, "/patterns/test.png").replace("/kyc_backend/kyc_backend/", "/kyc_backend/")]
    pattern = DetectANDReadID(patterns, path)
    print(pattern.start())
    print(pattern.counter)
    return pattern.read('front')

