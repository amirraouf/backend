import urllib

import face_recognition
import cv2
import numpy as np
import requests
from django.conf import settings
from .read_id import read_face_id
from kyc_sdk_api.v1.re_kyc.detect_and_read import DetectANDReadID



def _grab_image(path=None, stream=None, url=None):
    # if the path is not None, then load the image from disk
    if path is not None:
        image = cv2.imread(path)

    # otherwise, the image does not reside on disk
    else:
        # if the URL is not None, then download the image
        if url is not None:
            resp = requests.get(url)
            data = resp.text()

        # if the stream is not None, then the image has been uploaded
        elif stream is not None:
            data = stream.read()

        # convert the image to a NumPy array and then read it into
        # OpenCV format
        image = np.asarray(bytearray(data), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image


def extract_text_from_face_id(id_image, client=None):
    data = read_face_id(id_image.path)
    if data is None or (isinstance(data, dict) and len(data)==0):
        data.update({
            'flag':True,
            'reason': ''
        })
    else:
        data.update({
            'flag': False,
            'reason': ''
        })
    return data


def extract_text_from_back_id(id_image):
    data = {
        'status': 'أعزب',
        'religion': 'مسلم',
        'sex': 'ذكر',
        'issuance_date': '',
    }
    return data


def check_front_face_id_and_read(user):
    root = settings.APPS_DIR
    media = settings.MEDIA_ROOT
    image_path = "{}/{}".format(media, user.id_front_img)
    face_image_path = "{}/{}".format(media, user.face_img)
    patterns_face = ["{}{}".format(root, "/patterns/pattern_face.png").replace("/kyc_backend/kyc_backend/", "/kyc_backend/")]
    pattern = DetectANDReadID(patterns_paths=patterns_face, image_path=image_path)
    is_same_face = pattern.recognize_face(face_image_path)
    if is_same_face:
        is_front_id = pattern.start()
        if is_front_id:
            data = pattern.read('front')
            print(data)
            if data:
                return True, data, 0
            else:
                return False, {}, 2
        else:
            return False, {}, 1
    else:
        return False, {}, 0


def check_back_face_id_and_read(user):
    root = settings.APPS_DIR
    media = settings.MEDIA_ROOT
    image_path = "{}/{}".format(media, user.id_back_img)
    patterns = [
        "{}{}".format(root,"/patterns/backpattern.jpeg").replace("/kyc_backend/kyc_backend/", "/kyc_backend/"),
    ]
    pattern = DetectANDReadID(patterns, image_path=image_path)
    if pattern.start():
        data = pattern.read('back')
        print(data)
        if data:
            return True, data, 0
        else:
            return False, {}, 2
    else:
        return False, {}, 1
