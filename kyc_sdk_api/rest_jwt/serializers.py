import jwt

from calendar import timegm
from datetime import datetime, timedelta

from django.utils.translation import ugettext as _
from rest_framework import serializers

from rest_framework_jwt.settings import api_settings

from kyc_sdk_api.v1.mixins import StatusCodeSerializerMixin
from users.client.models import Client
from users.service_provider.models import APIKey
from kyc_sdk_api.rest_jwt.utils import (
    jwt_payload_handler,
    jwt_encode_handler,
    jwt_decode_handler,
    jwt_get_username_from_payload_handler
)


def validate_client(**data):
    try:
        apikey = APIKey.objects.get(key=data['api_key'])
    except APIKey.DoesNotExist:
        return False
    try:
        client = Client.objects.get(ref=data['ref'])
        return client if client.entity.api_key == apikey else False
    except Client.DoesNotExist:
        return False


class Serializer(StatusCodeSerializerMixin, serializers.Serializer):
    @property
    def object(self):
        return self.validated_data


class JSONWebTokenSerializer(Serializer):
    """
    Serializer class used to validate a ref and api_key.

    'ref' is identified by the client Model.ref.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """
    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields['ref'] = serializers.CharField()
        self.fields['api_key'] = serializers.CharField()

    @property
    def username_field(self):
        return 'ref'

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'api_key': attrs.get('api_key')
        }

        if all(credentials.values()):
            client = validate_client(**credentials)

            if client:
                if not client.is_valid:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(client, attrs.get('api_key'))
                return {
                    'token': jwt_encode_handler(payload),
                    'user': client.ref,
                }
            else:
                msg = _('Unable to authorize in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "api_key".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class VerificationBaseSerializer(Serializer):
    """
    Abstract serializer used for verifying and refreshing JWTs.
    """
    token = serializers.CharField()

    def validate(self, attrs):
        msg = 'Please define a validate method.'
        raise NotImplementedError(msg)

    def _check_payload(self, token):
        # Check payload valid (based off of JSONWebTokenAuthentication,
        # may want to refactor)
        try:
            payload = jwt_decode_handler(token)
        except jwt.ExpiredSignature:
            msg = _('Signature has expired.')
            raise serializers.ValidationError(msg)
        except jwt.DecodeError:
            msg = _('Error decoding signature.')
            raise serializers.ValidationError(msg)

        return payload

    def _check_user(self, payload):
        ref = jwt_get_username_from_payload_handler(payload)

        if not ref:
            msg = _('Invalid payload.')
            raise serializers.ValidationError(msg)

        # Make sure user exists
        try:
            client = Client.objects.get(ref=ref)
        except Client.DoesNotExist:
            msg = _("User doesn't exist.")
            raise serializers.ValidationError(msg)

        if not client.is_valid:
            msg = _('User account is disabled.')
            raise serializers.ValidationError(msg)

        return client


class VerifyJSONWebTokenSerializer(VerificationBaseSerializer):
    """
    Check the veracity of an access token.
    """

    def validate(self, attrs):
        token = attrs['token']

        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)

        return {
            'token': token,
            'user': user
        }


class RefreshJSONWebTokenSerializer(VerificationBaseSerializer):
    """
    Refresh an access token.
    """

    def validate(self, attrs):
        token = attrs['token']
        payload = self._check_payload(token=token)
        user = self._check_user(payload=payload)
        # Get and check 'orig_iat'
        orig_iat = payload.get('orig_iat')

        if orig_iat:
            # Verify expiration
            refresh_limit = api_settings.JWT_REFRESH_EXPIRATION_DELTA

            if isinstance(refresh_limit, timedelta):
                refresh_limit = (refresh_limit.days * 24 * 3600 +
                                 refresh_limit.seconds)

            expiration_timestamp = orig_iat + int(refresh_limit)
            now_timestamp = timegm(datetime.utcnow().utctimetuple())

            if now_timestamp > expiration_timestamp:
                msg = _('Refresh has expired.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('orig_iat field is required.')
            raise serializers.ValidationError(msg)

        new_payload = jwt_payload_handler(user, payload['api_key'])
        new_payload['orig_iat'] = orig_iat

        return {
            'token': jwt_encode_handler(new_payload),
            'user': user.ref
        }
