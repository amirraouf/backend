import datetime
from calendar import timegm

import jwt
from rest_framework_jwt.settings import api_settings

from users.client.models import Client
from users.service_provider.models import APIKey


def jwt_get_secret_key(payload=None):
    """
    For enhanced security you may want to use a secret key based on user.

    This way you have an option to logout only this user if:
        - token is compromised
        - password is changed
        - etc.
    """
    if api_settings.JWT_GET_USER_SECRET_KEY and payload is not None:
        entity = APIKey.objects.select_related('entity').get(key=payload.get('api_key')).entity
        key = str(jwt_get_user_secret_key(entity))
        return key
    return api_settings.JWT_SECRET_KEY


def jwt_get_username_from_payload_handler(payload):
    """
    Override this function if username is formatted differently in payload
    """
    return payload.get('ref')


def jwt_get_user_secret_key(entity):
    """
    Return the secret key specialized for the entity
    :param entity:
    :return:
    """
    return entity.secret_key.get_key()


def jwt_payload_handler(client, api_key):
    username_field = 'ref'
    username = client.ref

    payload = {
        'ref': client.ref,
        'api_key': api_key,
        'exp': datetime.datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }
    if hasattr(client, 'email'):
        payload['email'] = client.email
    payload['ref'] = str(client.ref)

    payload[username_field] = username

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = client.entity.name

    return payload


def jwt_encode_handler(payload):
    client = Client.objects.select_related('entity').get(ref=payload['ref'])
    key = client.entity.secret_key.get_key()
    return jwt.encode(
        payload,
        key,
        api_settings.JWT_ALGORITHM
    ).decode('utf-8')


def jwt_decode_handler(token):
    options = {
        'verify_exp': api_settings.JWT_VERIFY_EXPIRATION,
    }
    # get user from token, BEFORE verification, to get user secret key
    unverified_payload = jwt.decode(token, None, False)
    secret_key = jwt_get_secret_key(unverified_payload)
    return jwt.decode(
        token,
        secret_key,
        api_settings.JWT_VERIFY,
        options=options,
        leeway=api_settings.JWT_LEEWAY,
        audience=api_settings.JWT_AUDIENCE,
        issuer=api_settings.JWT_ISSUER,
        algorithms='HS256'
    )


def jwt_decode_payload_permission_handler(token, secret_key):
    payload = jwt.decode(
        token,
        secret_key,
        algorithms='HS256'
    )
    return payload['api_key']
