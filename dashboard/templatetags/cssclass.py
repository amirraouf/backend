from django import template
from django.forms import FileField

register = template.Library()


@register.filter(name='addclass')
def addcss(value, arg):
    css_classes = value.field.widget.attrs.get('class', None)
    if isinstance(value.field, FileField):
        return value
    if css_classes and arg not in css_classes:
        arg = '%s %s' % (css_classes, arg)
    return value.as_widget(attrs={'class': arg})


@register.filter(name='addplaceholder')
def addplaceholder(value, arg):
    if isinstance(value.field, FileField):
        return value
    return value.as_widget(attrs={'placeholder': arg})
