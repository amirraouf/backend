from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from guardian.decorators import permission_required
from test_plus.test import login

from users.baseuser.models import BaseUser


@login_required
def home(request):
    client_manager = request.user.entity.clients
    kyc_count = client_manager.filter(is_kyc=False).count()
    valid = client_manager.filter(is_valid=True)
    valid_count = valid.count()
    unseen = client_manager.unseen()
    unseen_count = unseen.count()
    return render(request, 'clients.html', context={
        'kyc_count': kyc_count,
        'valid_count': valid_count,
        'unseen_count': unseen_count,
        'clients': client_manager.all()
    })

@login_required
def verify(request, ref, flag):
    client = request.user.entity.clients.get(ref=ref)
    client.is_seen = True if flag=='t' else False
    client.save()
    return HttpResponse()

@login_required
def admins(request):
    admins = BaseUser.objects.filter(entity_id=request.user.entity.id)
