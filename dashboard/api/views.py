from rest_framework.generics import DestroyAPIView, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

from users.client.models import Client


class VerifyToggleAPIView(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        """
        Return a list of all users.
        """
        client = Client.objects.get(ref=request.GET.get('ref'))
        client.is_seen = not client.is_seen
        client.save()
        data = {'verify': client.is_seen}
        return Response(data)


class DeleteClientAPIView(DestroyAPIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    # authentication_classes = (authentication.SessionAuthentication,)
    authentication_classes = ()
    permission_classes = ()
    # permission_classes = (permissions.IsAuthenticated,)
    lookup_field = 'ref'

    def get_object(self):
        # import ipdb; ipdb.set_trace()
        filter_kwargs = {self.lookup_field: self.request.data[self.lookup_field]}
        obj = get_object_or_404(Client, **filter_kwargs)
        return obj
