from django.conf.urls import url
from .views import VerifyToggleAPIView, DeleteClientAPIView

urlpatterns = [
    url(r'^verify/$', VerifyToggleAPIView.as_view(), name='verify-toggle'),
    url(r'^delete/$', DeleteClientAPIView.as_view(), name='delete')
]
