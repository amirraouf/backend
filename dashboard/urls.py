from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import reverse

from .views import home
auth_views_urls = [
    url(r'^login/$', auth_views.login, {'template_name': 'login.html', 'redirect_authenticated_user': True},
        name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': None, 'template_name': 'logged_out.html'}, name='logout'),
    url(r'^password_reset/$', auth_views.password_reset,
        {'template_name': 'password-reset.html', 'post_reset_redirect': '/password_reset/done/'},
        name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete')
]

views_url = [
    url(r'^$', home, name='home')
]

urlpatterns = auth_views_urls + views_url
