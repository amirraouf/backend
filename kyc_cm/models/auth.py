# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings

TYPES = [
    (0, "Header"),
    (1, "Data")
]

# https://helpx.adobe.com/coldfusion/api-manager/authentication-types.html
# https://jwt.io/introduction/

class BasicAuth(models.Model):
    """
    Model represent authentication basic type
    :param username
    :param password
    """
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
    soap_auth_name = models.CharField(max_length=32, verbose_name='soap auth name',
                                      blank=True, null=True, help_text="Ignore it if you don't have soap requests")
    name_param = models.CharField(max_length=32, verbose_name='Username param name',
                                  help_text="Ignore it if you don't have soap requests", null=True)
    pass_param = models.CharField(max_length=32, verbose_name='Password param name',
                                  help_text="Ignore it if you don't have soap requests", null=True)
    typeof = models.PositiveSmallIntegerField(choices=TYPES, verbose_name="Type of")
    xml_root_tag_name = models.CharField(max_length=32, default='root', blank=True)
    user_created = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    url = models.ManyToManyField('kyc_cm.APIParam')

    def __str__(self):
        return "Basic Auth with " + TYPES[self.typeof][1]


class HMacAuth(models.Model):
    pass


class AccessTokenAuth(models.Model):
    pass


class OAuth2(models.Model):
    pass
