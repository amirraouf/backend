# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.validators import MaxValueValidator
from django.db import models

# Create your models here.
from django.conf import settings
from django.http import JsonResponse



class APIParam(models.Model):
    PURPOSES = [
        ("RE-kyc", "Internal KYC with Entity's client"),
    ]

    AUTH = [
        ("BA", "Basic Auth"),
    ]

    DATATYPES = [
        ("json", "JSON"),
        ("xml", "XML"),
        ("soap", "SOAP"),
    ]
    PLANS = [
        (0, "fail silently"),
        (1, "try again"),
        (2, "mark as dispute"),
    ]
    url = models.URLField(blank=True)
    purpose = models.CharField(max_length=6, choices=PURPOSES)
    file_category = models.ForeignKey("kyc_backend.entity.Config")
    user_created = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    authentication = models.CharField(max_length=4, choices=AUTH)
    data_type = models.CharField(max_length=4, choices=DATATYPES, default=0)
    soap_body_method = models.CharField(max_length=32, verbose_name='soap method name',
                                        blank=True, null=True, help_text="Ignore it if you don't have soap requests")
    action_plan = models.PositiveSmallIntegerField(default=0, choices=PLANS, help_text="It matters with the bill payment <b>ONLY</b>")
    timeout = models.PositiveSmallIntegerField(default=15, validators=[MaxValueValidator(25)])
    max_no_of_trials = models.PositiveSmallIntegerField(default=2, validators=[MaxValueValidator(3)])

    class Meta:
        unique_together = [
            ('purpose', 'user_created')
        ]
        verbose_name = "Api Parameters"

    def __str__(self):
        return self.purpose

    def auth_sys(self):
        if self.authentication == 'BA':
            from kyc_cm.models import BasicAuth
            try:
                return BasicAuth.objects.get(url=self)
            except BasicAuth.DoesNotExist:
                return None

    def headers_dict(self):
        names = self.headers.values_list('name', flat=True)
        values = self.headers.values_list('value', flat=True)
        return dict(zip(names, values))


class Headers(models.Model):
    name = models.CharField(max_length=32)
    value = models.CharField(max_length=32)
    basic_auth = models.ForeignKey('kyc_cm.APIParam', related_name='headers')

    def __str__(self):
        return 'header ' + self.name


class BodyDesign(models.Model):
    request_body = models.TextField()
    response_body = models.TextField()
    apiparam = models.OneToOneField('kyc_cm.APIParam', related_name='body')

    def __str__(self):
        return 'Design of ' + self.apiparam.purpose

    def return_request_structure(self, headers, body, auth_sys):
        auth = {auth_sys.name_param: auth_sys.username, auth_sys.pass_param: auth_sys.password}
        body_structure = self.request_body
        headers_auth = headers.copy()
        headers_auth.update(auth)
        body_auth = body.copy()
        body_auth.update(auth)
        body_structure = body_structure.replace('\'<header>\'', str(headers))
        body_structure = body_structure.replace('\'<body>\'', str(body))
        body_structure = body_structure.replace('<body>', str(body)[1:-1])
        body_structure = body_structure.replace('\'<headers_auth>\'', str(headers_auth))
        body_structure = body_structure.replace('\'<body_auth>\'', str(body_auth))
        body_structure = body_structure.replace('<body_auth>', str(body_auth)[1:-1])
        return eval(body_structure)

    def return_response_structure(self, response, purpose=None):
        body_structure = self.response_body
        if body_structure == '{}':
            return response

        if purpose and self.apiparam.file_category.clientinquiryrequest.has_many:
            body_structure = body_structure.replace('\'<header>\'', str({}))
            import re
            list_search = re.search('/.*>', body_structure)
            list_id = body_structure[list_search.start() + 1: list_search.end() - 1]
            list_key, list_attr = list_id.split('/')
            body_structure = re.sub('<body/.*>', 'BODY', body_structure)
            # to see where is the list of bills we will use / after body to know what is the key
            body_structure = dict(eval(body_structure))

            for data in body_structure:
                if body_structure[data] == 'BODY':
                    try:
                        response_body = response[data]
                        list_data = response_body.pop(list_key)
                        list_data = list_data[list_attr]

                        for elem in list_data:
                            elem.update(response_body)
                        return list_data

                    except KeyError:
                        return JsonResponse({'message': 'Response data is not valid'}, status=400)
        else:
            body_structure = body_structure.replace('\'<header>\'', str({}))
            body_structure = body_structure.replace('\'<body>\'', '\'BODY\'')
            body_structure = dict(eval(body_structure))
            for data in body_structure:
                if body_structure[data] == 'BODY':
                    try:
                        return response[data]
                    except KeyError:
                        return JsonResponse({'message': 'Response data is not valid'}, status=400)


class APIResponse(models.Model):
    """
    Response to Client Inquiry Request for setting a 404 cases
    """
    param_name = models.CharField(max_length=32, verbose_name='Unique parameter')
    param_value = models.CharField(max_length=32, verbose_name='Value', blank=True,
                                   help_text='It is required for identifying the error case')
    apiparam = models.ForeignKey('kyc_cm.APIParam', related_name='responses', null=True)
    parent_response = models.ForeignKey('self', related_name='responses', null=True)
    show_in_case_of_error = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Api responses"

    def __str__(self):
        if self.apiparam:
            return self.param_name + ' for ' + self.apiparam.purpose
        else:
            return self.param_name + ' for ' + self.parent_response.param_name + ' ' + self.parent_response.apiparam.purpose

