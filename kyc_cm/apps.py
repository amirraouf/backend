# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class PCMConfig(AppConfig):
    name = 'kyc_cm'
    verbose_name = 'Communication Module'
