import datetime
import json
from urllib2 import URLError
from xml.etree.ElementTree import fromstring, ParseError

import logging
import requests
from requests import ConnectionError
from requests import HTTPError
from rest_framework import status
from rest_framework_xml.renderers import XMLRenderer
from suds.client import Client
from rest_framework_xml.parsers import XMLParser
from django.utils.six import BytesIO

from django.http import Http404, JsonResponse

from kyc_cm.models import APIResponse
from kyc_cm.models import BasicAuth

from data.models import FileCategory

PCM = logging.getLogger("pcm")


class APIConnect(object):
    def __init__(self, category, purpose):
        if type(category) is int:
            self.category = FileCategory.objects.get(id=category)
        else:
            self.category = category
        self.purpose = purpose

    def apiparam(self):
        try:
            apiparam = self.category.apiparam_set.get(purpose=self.purpose)
            return apiparam
        except:
            raise Http404

    def basic_auth_sys(self):
        apiparam = self.apiparam()

        auth_sys = apiparam.auth_sys()
        return apiparam, auth_sys

    def soap_connect(self, serialized_data, apiparam, auth_sys):
        if isinstance(auth_sys, BasicAuth):
            try:
                client = Client(str(apiparam.url))  # call the client
                auth = client.factory.create(auth_sys.soap_auth_name)  # create authentication factory
                auth[auth_sys.name_param] = auth_sys.username
                auth[auth_sys.pass_param] = auth_sys.password
                client.set_options(soapheaders=auth)  # set auth for headers options
                client.set_options(headers=apiparam.headers_dict())
                method = getattr(client.service, apiparam.soap_body_method)
                try:
                    resp_obj = method(**serialized_data)
                except Exception as e:
                    PCM.debug('TimeOut Error' + ' ' + str(e.message) + ' ' + datetime.datetime.now().strftime(
                        "%d/%m/%Y %H:%M")
                              + '\n' + str(serialized_data))
                    return JsonResponse({'message': 'Runtime error happened from the client'},
                                        status=status.HTTP_408_REQUEST_TIMEOUT)
            except URLError:
                return JsonResponse({'message': 'Runtime error happened from the client'},
                                    status=status.HTTP_408_REQUEST_TIMEOUT)

            if resp_obj == 1:
                return resp_obj
            attributes = dir(resp_obj)  # return list of names comprising (some of) the attributes
            identifiers = filter(lambda i: not i.startswith("__"),
                                 attributes)  # filter attributes start with dunder
            resp_data = dict(zip(
                identifiers, map(lambda x: unicode(resp_obj[x]), identifiers))
            )  # Convert data object to dictionary
            PCM.debug(datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + '----> SOAP Response for '
                      + self.category.user_created.username + ' <-- \n' + str(resp_data))
            if self.purpose == "CIReq":
                responses_checkers = self.check_responses(apiparam, resp_data)
                if isinstance(responses_checkers, JsonResponse):
                    return responses_checkers

            return resp_data
        else:
            return JsonResponse({'message': 'Authentication system not found'},
                                status=status.HTTP_401_UNAUTHORIZED)

    def json_connect(self, serialized_data, apiparam, auth_sys, trial=0):
        if isinstance(auth_sys, BasicAuth):
            headers = {'Content-Type': 'application/json'}

            if auth_sys.typeof == 0:
                headers.update(apiparam.headers_dict())
                headers.update({auth_sys.name_param: auth_sys.username, auth_sys.pass_param: auth_sys.password})

            requested_body = apiparam.body.return_request_structure(headers=apiparam.headers_dict(),
                                                                    body=serialized_data,
                                                                    auth_sys=auth_sys)
            try:
                resp = requests.post(str(apiparam.url), json=requested_body, headers=headers, timeout=apiparam.timeout)
                if resp.status_code == 404:
                    return JsonResponse({'message': 'Detail Not Found'},
                                        status=status.HTTP_404_NOT_FOUND)
                try:
                    resp_data = resp.json()
                    resp_data.update(serialized_data)
                except ValueError:
                    return JsonResponse({'message': 'Biller data are not proper'},
                                        status=status.HTTP_404_NOT_FOUND)
                PCM.debug(datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + '----> Json Response for '
                          + self.category.user_created.username + ' <-- \n' + str(resp_data))
                if self.purpose == "CIReq":
                    responses_checkers = self.check_responses(apiparam, resp_data)
                    if isinstance(responses_checkers, JsonResponse):
                        return responses_checkers
                    return apiparam.body.return_response_structure(resp_data, self.purpose)
                else:
                    return apiparam.body.return_response_structure(resp_data)
            except requests.exceptions.Timeout:
                if self.purpose == "BPResp":
                    if apiparam.action_plan == 0:  # fail silently
                        return JsonResponse({'message': 'Runtime error happened from the client'},
                                            status=status.HTTP_408_REQUEST_TIMEOUT)
                    elif apiparam.action_plan == 1:  # try again
                        if trial <= apiparam.max_no_of_trials:
                            return self.json_connect(serialized_data=serialized_data,
                                                     apiparam=apiparam,
                                                     auth_sys=auth_sys,
                                                     trial=trial + 1)
                    else:
                        return True

                elif self.purpose != "CIReq":
                    if trial <= apiparam.max_no_of_trials:
                        return self.json_connect(serialized_data=serialized_data,
                                                 apiparam=apiparam,
                                                 auth_sys=auth_sys,
                                                 trial=trial + 1)
                else:
                    trial += 1
                    if trial < 3:
                        return self.json_connect(serialized_data=serialized_data,
                                                 apiparam=apiparam,
                                                 auth_sys=auth_sys,
                                                 trial=trial)
                return JsonResponse({'message': 'An Error occurred while notifying the biller'},
                                    status=status.HTTP_403_FORBIDDEN)

        else:
            return JsonResponse({'message': 'Authentication system not found'},
                                status=status.HTTP_401_UNAUTHORIZED)

    def xml_connect(self, serialized_data, apiparam, auth_sys, trial=0):
        if isinstance(auth_sys, BasicAuth):
            renderer = XMLRenderer()
            renderer.root_tag_name = auth_sys.xml_root_tag_name
            headers = {'Content-Type': 'application/xml'}

            if auth_sys.typeof == 0:
                headers.update(apiparam.headers_dict())
                headers.update({auth_sys.name_param: auth_sys.username, auth_sys.pass_param: auth_sys.password})

            elif auth_sys.typeof == 1:
                serialized_data = apiparam.body.return_request_structure(headers=apiparam.headers_dict(),
                                                                         body=serialized_data,
                                                                         auth_sys=auth_sys)
            try:
                resp = requests.post(str(apiparam.url), data=renderer.render(serialized_data),
                                     headers=headers, timeout=apiparam.timeout)
            except requests.exceptions.Timeout:
                if self.purpose == "BPResp":
                    if apiparam.action_plan == 0:  # fail silently
                        return JsonResponse({'message': 'Runtime error happened from the client'},
                                            status=status.HTTP_408_REQUEST_TIMEOUT)
                    elif apiparam.action_plan == 1:  # try again
                        if trial <= apiparam.max_no_of_trials:
                            return self.xml_connect(serialized_data=serialized_data,
                                                    apiparam=apiparam,
                                                    auth_sys=auth_sys,
                                                    trial=trial + 1)
                    else:
                        return True

                elif self.purpose != "CIReq":
                    if trial <= apiparam.max_no_of_trials:
                        return self.json_connect(serialized_data=serialized_data,
                                                 apiparam=apiparam,
                                                 auth_sys=auth_sys,
                                                 trial=trial + 1)
                else:
                    trial += 1
                    if trial < 3:
                        return self.xml_connect(serialized_data=serialized_data,
                                                apiparam=apiparam,
                                                auth_sys=auth_sys,
                                                trial=trial)
                return JsonResponse({'message': 'An Error occurred while notifying the biller'},
                                    status=status.HTTP_403_FORBIDDEN)

            def _type_convert(value):  # Monkey patching
                if value is None:
                    return value
                try:
                    return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
                except ValueError:
                    pass

                try:
                    return str(value)
                except ValueError:
                    return value

            # xml = BytesIO(resp.content)
            # parser = XMLParser()
            # parser._type_convert = _type_convert
            try:
                from xmljson import Parker
                parker = Parker(simple_text=False, xml_fromstring=False)
                resp_data = json.loads(json.dumps(parker.data(fromstring(resp.content))))
                PCM.debug(datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + '----> XML Response for '
                          + self.category.user_created.username + ' <-- \n' + str(resp_data))
            except ParseError:
                return JsonResponse({'message': 'Error occurs from client server'},
                                    status=resp.status_code)
            responses_checkers = self.check_responses(apiparam, resp_data)
            if isinstance(responses_checkers, JsonResponse):
                return responses_checkers
            if self.purpose == "CIReq":
                return apiparam.body.return_response_structure(resp_data, self.purpose)
            else:
                try:
                    return apiparam.body.return_response_structure(resp_data)
                except:
                    return resp_data

        else:
            return JsonResponse({'message': 'Authentication system not found'},
                                status=status.HTTP_401_UNAUTHORIZED)

    @staticmethod
    def check_responses(apiparam, resp_data):
        for response in apiparam.responses.all():
            try:
                if response.responses.count() > 0:
                    for nested_response in response.responses.all():
                        try:

                            if resp_data[response.param_name][nested_response.param_name] == type(
                                    resp_data[response.param_name][nested_response.param_name])(
                                nested_response.param_value):
                                PCM.debug(
                                    datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + '----> Check Res <-- \n' + str(
                                        resp_data))
                                try:
                                    error_message_response = apiparam.responses.get(show_in_case_of_error=True)
                                    # Check if there's a response message if error happens
                                    error_message_field = error_message_response.param_name  # name of message key
                                    if error_message_response.parent_response:  # check if it is nested or not
                                        parent_message_key = error_message_response.parent_response.param_name
                                        message = resp_data[parent_message_key][error_message_field]
                                    else:
                                        message = resp_data[error_message_field]
                                    return JsonResponse({'message': message}, status=404)
                                except APIResponse.DoesNotExist:
                                    return JsonResponse({'message': 'Something went wrong'}, status=404)

                        except (ValueError, KeyError, TypeError) as e:
                            return JsonResponse({'message': 'Responses are not handled yet!'},
                                                status=406)
                else:
                    try:
                        if resp_data[response.param_name] == type(resp_data[response.param_name])(
                                response.param_value):
                            try:
                                error_message_response = apiparam.responses.get(show_in_case_of_error=True)
                                # Check if there's a response message if error happens
                                error_message_field = error_message_response.param_name  # name of message key
                                if error_message_response.parent_response:  # check if it is nested or not
                                    parent_message_key = error_message_response.parent_response.param_name
                                    message = resp_data[parent_message_key][error_message_field]
                                else:
                                    message = resp_data[error_message_field]
                                return JsonResponse({'message': message}, status=404)
                            except APIResponse.DoesNotExist:
                                return JsonResponse({'message': 'Something went wrong'},
                                                    status=404)
                    except:
                        pass
            except (KeyError, TypeError) as e:
                if isinstance(e, KeyError):
                    try:
                        if resp_data[response.param_name] == type(resp_data[response.param_name])(
                                response.param_value):
                            PCM.debug(
                                datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + '----> CHeck Res <-- \n' + str(
                                    resp_data))
                            try:
                                error_message_response = apiparam.responses.get(show_in_case_of_error=True)
                                # Check if there's a response message if error happens
                                error_message_field = error_message_response.param_name  # name of message key
                                if error_message_response.parent_response:  # check if it is nested or not
                                    parent_message_key = error_message_response.parent_response.param_name
                                    message = resp_data[parent_message_key][error_message_field]
                                else:
                                    message = resp_data[error_message_field]
                                return JsonResponse({'message': message}, status=404)
                            except APIResponse.DoesNotExist:
                                return JsonResponse({'message': 'Something went wrong'},
                                                    status=404)
                    except KeyError:
                        return JsonResponse({'message': 'Responses are not handled yet!'}, status=406)
