from django.http import Http404, JsonResponse
from rest_framework import status

from .connect import APIConnect

from kyc_cm.models import ClientInquiryRequest

from data.cronjobs.sms_reminder import SendSMS


class BillInquiryConnect(APIConnect):

    def __init__(self, category, purpose, data, aggregator, pay_fine, client_data=None):
        super(BillInquiryConnect, self).__init__(category, purpose)
        self.data = data
        self.aggregator = aggregator
        self.pay_fine = pay_fine
        self.client_data = client_data

    def connect(self):
        apiparam, auth_sys = self.basic_auth_sys()
        if self.purpose == 'VAReq':  # Validation Required
            serialized_data = self.data
            serialized_data.update(self.client_data)
            method = getattr(self, '%s_connect' % apiparam.data_type)
            resp_data = method(serialized_data, apiparam, auth_sys)
            # Return here differs as it must return tuple of validation as validate function at Fine model
            if isinstance(resp_data, JsonResponse):
                return False, resp_data
            else:
                return True, resp_data
        else:  # Client Inquiry Request
            ciresp = ClientInquiryRequest.objects.get(file_category=self.category)
            serialized_data = ciresp.map_data(self.data)
            if isinstance(serialized_data, JsonResponse):
                return serialized_data
            method = getattr(self, '%s_connect' % apiparam.data_type)  # xml - json - soap
            resp_data = method(serialized_data, apiparam, auth_sys)
            if isinstance(resp_data, JsonResponse):
                return resp_data
            not_accepted_flag = True  # Flag for partial or over flow acceptance
            if ciresp.no_phone_number:
                if resp_data.has_key(ciresp.mobile_no):
                    if resp_data[ciresp.mobile_no] == 'None':
                        return JsonResponse({'message': 'Mobile Number is not satisfied'}, status=status.HTTP_406_NOT_ACCEPTABLE)

            if self.data.get('due_amount', None):  # TODO:No due_amount is sent till now from the wallet portal 27/09/2017
                if self.pay_fine:
                    amount = float(resp_data[self.category.fine_field])
                else:
                    amount = float(resp_data[self.category.payable_amount_field])
                if self.category.can_overpay or self.category.has_partial_payment:
                    if amount > float(self.data['due_amount']):
                        if ciresp.check_over(resp_data[ciresp.over_field]):
                            not_accepted_flag = False
                    if amount < float(self.data['due_amount']):
                        if ciresp.check_partial(resp_data[ciresp.partial_field]):
                            not_accepted_flag = False

                if not_accepted_flag and self.category.has_reminder:
                    return JsonResponse({"message": "Amount is not acceptable by the merchant"}, status=status.HTTP_406_NOT_ACCEPTABLE)
            resp_data['due_amount'] = resp_data[self.category.amount_field]
            resp_data['mobile_no'] = resp_data.get(self.category.mobile_field, None)
            return resp_data
