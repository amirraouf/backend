from django.http import JsonResponse

from sharing.settings.celery import app
from .connect import APIConnect

from kyc_cm.models import BillPaymentRequest


class BillPaymentConnect(APIConnect):
    def __init__(self, category, purpose, data, client_data):
        super(BillPaymentConnect, self).__init__(category, purpose)
        self.data = data
        self.client_data = client_data

    def connect(self):
        apiparam, auth_sys = self.basic_auth_sys()
        bpresp = BillPaymentRequest.objects.get(file_category=self.category)
        serialized_data = bpresp.map_data(self.data, self.client_data)

        if isinstance(serialized_data, JsonResponse):
            return serialized_data
        if apiparam.data_type == 'soap':
            from suds.sax.date import DateTime
            serialized_data[bpresp.datetime] = DateTime(serialized_data[bpresp.datetime].replace(' ', 'T'))
        method = getattr(self, '%s_connect' % apiparam.data_type)
        resp_data = method(serialized_data, apiparam, auth_sys)
        return resp_data
