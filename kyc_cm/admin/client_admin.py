# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .mixins import HierarchyQuerySetAdminModel


class ClientInquiryRequestAdmin(HierarchyQuerySetAdminModel, admin.ModelAdmin):
    pass

