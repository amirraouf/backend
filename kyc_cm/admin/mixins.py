from django.core.exceptions import FieldError
from django.forms.models import BaseInlineFormSet
from django.forms.models import ModelForm
from django.contrib import admin

class HierarchyQuerySetAdminModel(object):
    exclude = ('user_created',)

    def get_exclude(self, request, obj=None):
        exclude = super(HierarchyQuerySetAdminModel, self).get_exclude(request, obj)
        if request.user.is_superuser:
            return ()
        return exclude

    def get_queryset(self, request):
        try:
            hierarchy = request.user.hierarchy
            qs = super(HierarchyQuerySetAdminModel, self).get_queryset(request)
            return qs.filter(user_created__hierarchy_id=hierarchy)
        except FieldError:
            return super(HierarchyQuerySetAdminModel, self).get_queryset(request)

    def render_change_form(self, request, context, *args, **kwargs):
        try:
            qs = context['adminform'].form.fields['file_category'].queryset
            context['adminform'].form.fields['file_category'].queryset = qs.filter(user_created__hierarchy_id=request.user.hierarchy)
            return super(HierarchyQuerySetAdminModel, self).render_change_form(request, context, args, kwargs)
        except KeyError:
            return super(HierarchyQuerySetAdminModel, self).render_change_form(request, context, args, kwargs)

    def save_form(self, request, form, change):
        instance = form.save(commit=False)
        if not change:
            instance.user_created = request.user if request.user.is_parent else request.user.parent
            instance.save()
        return instance


class CustomInlineFormset(BaseInlineFormSet):
    """
    Custom formset that support initial data
    """

    def initial_form_count(self):
        """
        set 0 to use initial_extra explicitly.
        """
        if self.initial_extra:
            return 0
        else:
            return BaseInlineFormSet.initial_form_count(self)

    def total_form_count(self):
        """
        here use the initial_extra len to determine needed forms
        """
        if self.initial_extra:
            count = len(self.initial_extra) if self.initial_extra else 0
            count += self.extra
            return count
        else:
            return BaseInlineFormSet.total_form_count(self)


class CustomModelForm(ModelForm):
    """
    Custom model form that support initial data when save
    """

    def has_changed(self):
        """
        Returns True if we have initial data.
        """
        has_changed = ModelForm.has_changed(self)
        return bool(self.initial or has_changed)


class CustomInlineAdmin(admin.TabularInline):
    """
    Custom inline admin that support initial data
    """
    form = CustomModelForm
    formset = CustomInlineFormset
