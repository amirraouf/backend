from django.contrib import admin


class AggregatorResponseAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(AggregatorResponseAdmin, self).get_queryset(request)
        qs = qs.filter(biller__biller__hierarchy_id=request.user.hierarchy)
        return qs