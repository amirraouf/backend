# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from kyc_cm.models import Headers, BodyDesign, APIResponse
from .mixins import HierarchyQuerySetAdminModel, CustomInlineAdmin


class APIResponseInline(CustomInlineAdmin):
    model = APIResponse
    extra = 0
    exclude = ('parent_response',)

    def get_queryset(self, request):
        qs = super(APIResponseInline, self).get_queryset(request)
        return qs.filter(parent_response=None)


class APIResponseNestedInline(CustomInlineAdmin):
    model = APIResponse
    extra = 0
    exclude = ('apiparam',)


class APIResponseNestedAdmin(admin.ModelAdmin):
    inlines = [APIResponseNestedInline]
    exclude = ('parent_response', 'apiparam')

    def get_queryset(self, request):
        qs = super(APIResponseNestedAdmin, self).get_queryset(request)
        return qs.filter(apiparam__user_created__hierarchy_id=request.user.hierarchy, parent_response=None)

    def save_formset(self, request, form, formset, change):
        if formset.is_valid():
            responses = formset.save(commit=False)
            for response in responses:
                response.apiparam = response.parent_response.apiparam
                response.save()


class HeaderElementFormSet(BaseInlineFormSet):
    """
    Validate formset names here
    """
    def clean(self):
        super(HeaderElementFormSet, self).clean()

        names = []
        for form in self.forms:
            if not hasattr(form, 'cleaned_data'):
                continue
            name = form.cleaned_data['name']
            if name in names:
                raise ValidationError('%s is duplicated' % name)
            names.append(form.cleaned_data['name'])


class ChoiceHeadersInline(CustomInlineAdmin):
    model = Headers
    formset = HeaderElementFormSet
    extra = 0


class BodyDesignInline(CustomInlineAdmin):
    model = BodyDesign
    extra = 0


class APIParamAdmin(HierarchyQuerySetAdminModel, admin.ModelAdmin):
    inlines = [ChoiceHeadersInline, BodyDesignInline, APIResponseInline]

    def render_change_form(self, request, context, *args, **kwargs):
        qs = context['adminform'].form.fields['file_category'].queryset
        context['adminform'].form.fields['file_category'].queryset = qs.filter(user_created__hierarchy_id=request.user.hierarchy)
        return super(APIParamAdmin, self).render_change_form(request, context, args, kwargs)
