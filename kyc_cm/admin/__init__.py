from django.contrib import admin

from kyc_cm.admin.aggregator_responses_admin import AggregatorResponseAdmin
from .bill_payment_admin import BillPaymentRequestAdmin
from .apiparam_admin import APIParamAdmin, APIResponseNestedAdmin
from .auth_admin import BasicAuthAdmin
from .client_admin import ClientInquiryRequestAdmin
from kyc_cm.models import *
from .mixins import HierarchyQuerySetAdminModel, CustomInlineAdmin

admin.site.register(APIParam, APIParamAdmin)
admin.site.register(BasicAuth, BasicAuthAdmin)
admin.site.register(BillPaymentRequest, BillPaymentRequestAdmin)
admin.site.register(ClientInquiryRequest, ClientInquiryRequestAdmin)
admin.site.register(APIResponse, APIResponseNestedAdmin)
admin.site.register(AggregatorResponse, AggregatorResponseAdmin)

__all__ = [
    'HierarchyQuerySetAdminModel',
    'CustomInlineAdmin'
]