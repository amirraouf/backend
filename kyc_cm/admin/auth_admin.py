# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db.models import Q

from kyc_cm.models import APIParam
from .mixins import HierarchyQuerySetAdminModel


# Register your models here.

class BasicAuthAdmin(HierarchyQuerySetAdminModel, admin.ModelAdmin):

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        qs = kwargs.get('queryset', db_field.rel.to.objects)
        if db_field.name == 'url':
            user = request.user if request.user.is_parent else request.user.parent
            qs = APIParam.objects.filter(Q(user_created=user) &
                                         Q(authentication='BA'))
            kwargs['queryset'] = qs

        return super(BasicAuthAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

