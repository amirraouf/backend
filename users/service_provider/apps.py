from django.apps import AppConfig


class EntityConfig(AppConfig):
    name = 'users.service_provider'
