from django.contrib import admin
from django.contrib import messages

from users.service_provider.models import APIKey, License, Config, SecretKey, Entity
from utils.auth_utils import generate_api_key


class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created', 'modified')

    fieldsets = (
        ('Required Information', {'fields': ('name', 'entity')}),
        ('Additional Information', {'fields': ('key_message',)}),
    )
    readonly_fields = ('key_message',)

    search_fields = ('id', 'name',)

    def has_delete_permission(self, request, obj=None):
        return False

    def key_message(self, obj):
        if obj.key:
            return "Hidden"
        return "The API Key will be generated once you click save."

    def save_model(self, request, obj, form, change):
        if not obj.key:
            obj.key = generate_api_key()
            messages.add_message(request, messages.WARNING, ('The API Key for %s is %s. Please note it since you will '
                                                             'not be able to see it again.' % (obj.name, obj.key)))
        obj.save()


class SecretKeyAdmin(admin.ModelAdmin):
    exclude = ('is_encrypted',)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return 'key',
        return ()

    def save_model(self, request, obj, form, change):
        super(SecretKeyAdmin, self).save_model(request, obj, form, change)
        obj.set_key()

admin.site.register(model_or_iterable=APIKey, admin_class=ApiKeyAdmin)
admin.site.register(model_or_iterable=SecretKey, admin_class=SecretKeyAdmin)
admin.site.register(Config)
admin.site.register(License)
admin.site.register(Entity)
