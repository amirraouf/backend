import uuid

from django.core.validators import MinLengthValidator
from django.db import models
from model_utils.models import TimeStampedModel
import environ
from cryptography.fernet import Fernet
from Crypto.Cipher.AES import AESCipher

env = environ.Env()
secret_key = env.str('SECRET_KEY')


class SecretKey(TimeStampedModel):
    """
    THis model represents the secret key to encrypt/decrypt the JWT
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    entity = models.OneToOneField("service_provider.Entity", related_name='secret_key')
    key = models.CharField(max_length=32, unique=True, validators=[MinLengthValidator(32)])

    is_encrypted = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Secret Keys"
        ordering = ['-created']

    def __str__(self):
        return self.entity.name

    def set_key(self):
        # TODO: Encrypt the key
        # key = AESCipher(secret_key.encode())
        # cipher = key.encrypt(self.key.encode())
        # self.key = cipher.decode('latin-1').encode('utf-8')
        self.is_encrypted = True
        self.save()

    def get_key(self):
        # key = AESCipher(secret_key.encode())
        # deciphered = key.decrypt(bytes(self.key).decode('utf-8').encode('latin-1'))
        return self.key
