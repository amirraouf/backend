import uuid

from django.db import models


class Entity(models.Model):
    """
    Represents any entity, banks or any corporate, It is the main
    User and all models related to it.
    """
    name = models.CharField(max_length=128, unique=True)
    ref = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.name
