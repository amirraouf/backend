import uuid

from django.conf import settings
from django.db import models
from model_utils import FieldTracker


class License(models.Model):
    """
    Magic identifier (to verifiy that it is a license file for the program)
    License id
    Customer id
    License create date
    License expire date (to check if it is expired)
    Extra fields, e.g. unlocked features
    """
    license_id = models.UUIDField(default=uuid.uuid4, primary_key=True, auto_created=True)
    entity = models.OneToOneField("service_provider.Entity", related_name='license')
    create_date = models.DateField(auto_now_add=True)
    expiry_date = models.DateField()
    quota_number = models.IntegerField(default=1000)
    auto_renewal = models.BooleanField(default=False)
    tracker = FieldTracker(fields=['quota_number'])

    default_renewal_quota = models.IntegerField(default=1000)

    def __str__(self):
        return self.entity.name

    def renew_quota(self):
        self.quota_number = self.default_renewal_quota
        self.save()

    def is_valid(self):
        return True if self.quota_number > 0 else False

    def use_license(self):
        if self.is_valid():
            self.quota_number -= 1
            self.save()
            return True
        if self.auto_renewal:
            self.renew_quota()
            self.use_license()
        return False

    def retrieve_license(self):
        self.quota_number += 1
        self.save()
