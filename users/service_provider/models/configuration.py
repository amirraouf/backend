from django.db import models


class Config(models.Model):
    entity = models.OneToOneField('service_provider.Entity')
