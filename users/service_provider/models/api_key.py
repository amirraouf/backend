import uuid
from django.db import models
from model_utils.models import TimeStampedModel


class APIKey(TimeStampedModel):
    """
    THis model represents the API KEY provided by Product owner to service provider
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=50, unique=True)
    key = models.CharField(max_length=40, unique=True)
    entity = models.OneToOneField("service_provider.Entity", related_name='api_key')

    class Meta:
        verbose_name_plural = "API Keys"
        ordering = ['-created']

    def __str__(self):
        return self.name
