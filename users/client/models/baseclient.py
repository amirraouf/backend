from django.contrib.postgres.fields import ArrayField
from django.db import models
from model_utils import FieldTracker
from model_utils.fields import SplitField
from django.utils.crypto import get_random_string

from model_utils.models import TimeStampedModel
from kyc_backend.utils import pkgen, update_filename

from django.utils.timezone import now


class BaseClient(TimeStampedModel):
    GENDER = (
        (0, 'Male'),
        (1, 'FEMale')
    )
    RELIGION = (
        (0, 'Muslim'),
        (1, 'Christian'),
        (2, 'Jewish'),
    )
    STATUS = (
        (0, 'Single'),
        (1, 'Married'),
        (2, 'Divorced')
    )
    is_valid = models.BooleanField(default=False, verbose_name="Valid data")
    face_img = models.ImageField(upload_to=update_filename, null=True)
    mobile_no = models.CharField(max_length=11, null=True)
    full_name = models.CharField(max_length=128, null=True)
    address = models.CharField(null=True, max_length=256)
    national_id = models.CharField(max_length=14, null=True)
    gender = models.PositiveSmallIntegerField(choices=GENDER, default=0, null=True)
    religion = models.PositiveSmallIntegerField(choices=RELIGION, default=0, null=True)
    status = models.PositiveSmallIntegerField(choices=STATUS, default=0, null=True)
    work = models.CharField(max_length=256, default=0)
    ref = models.CharField(default=pkgen, max_length=10, unique=True)
    tracker = FieldTracker(fields=['mobile_no', 'address', 'is_seen'])
    is_kyc = models.BooleanField(default=False)
    otp = models.CharField(max_length=6, null=True)
    otp_timer = models.DateTimeField(null=True)
    available_mobile_no = ArrayField(models.CharField(max_length=11), size=10, null=True, default=[])

    def generate_otp(self):
        otp = get_random_string(length=6, allowed_chars='0123456789')
        self.otp = otp
        self.otp_timer = now()
        self.save()
        return otp

    def check_otp(self, otp):
        return self.otp == otp and (now() - self.otp_timer).total_seconds() < 901

    class Meta:
        abstract = True
