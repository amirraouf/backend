from django.db import models

from kyc_backend.utils import update_filename
from .baseclient import BaseClient
from django.utils.crypto import get_random_string
from django.utils.timezone import now

class ClientManager(models.Manager):
    def unseen(self):
        query = self.get_queryset()
        return query.filter(is_seen=False)

class Client(BaseClient):
    entity = models.ForeignKey("service_provider.Entity", related_name='clients')
    no_of_sendings = models.PositiveSmallIntegerField(default=0)
    mobile_device = models.OneToOneField('client.MobileDevice', null=True)
    is_seen = models.BooleanField(default=False, verbose_name="Is verified by entity?")
    objects = ClientManager()
    id_back_img = models.ImageField(upload_to=update_filename, null=True)
    id_front_img = models.ImageField(upload_to=update_filename, null=True)

    def verify(self, face_text, back_text):
        self.is_kyc = True
        self.save()
        return True

    def send_sms(self):
        pass

    def generate_otp(self):
        otp = get_random_string(length=6, allowed_chars='0123456789')
        self.otp = otp
        self.otp_timer = now()
        self.no_of_sendings += 1
        self.save()
        return otp

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.is_seen and self.is_valid:
            self.is_kyc = True
        super(Client, self).save(force_insert, force_update, using, update_fields)

    def delete(self, using=None, keep_parents=False):
        self.entity.license.retrieve_license() if not self.is_valid else None
        super(Client, self).delete(using, keep_parents)
