from django.contrib import admin

# Register your models here.
from users.baseuser.models import BaseUser

admin.site.register(BaseUser)
