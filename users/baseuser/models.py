from django.contrib.auth.models import AbstractUser
from django.db import models
from model_utils.models import TimeStampedModel


class BaseUser(AbstractUser, TimeStampedModel):
    moderator = 1
    auditor = 2
    admin = 3
    supervisor = 4

    USER_TYPE_CHOICES = (
        (moderator, 'Moderator'),
        (auditor, 'Auditor'),
        (admin, 'Admin'),
        (supervisor, 'Supervisor'),
    )

    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)
    entity = models.ForeignKey("service_provider.Entity", null=True)
    mobile_no = models.CharField(max_length=14, blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    profile_pic = models.ImageField(upload_to='profile_pics', default='dist/img/avatar.png/')
