import datetime
import os
import random
import string

from django.contrib.auth.base_user import BaseUserManager


def update_filename(instance, filename):
    """
    update document name
    :param instance: doc instance
    :param filename: filename of uploaded file
    :return:
    """
    now = datetime.datetime.now()
    path = "imgs/{0}/{1}/{2}".format(now.year, now.month, instance.entity.name)
    file, ext = filename.split('.')
    filename = file + '_' + instance.ref + '_' + \
               ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(9)) + \
               '.' + ext
    return os.path.join(path, filename)


def generate_otp():
    return BaseUserManager().make_random_password(length=6, allowed_chars='0123456789')


def pkgen():
    from base64 import b32encode
    from hashlib import sha1
    pk = b32encode(sha1(str(random.random()).encode('utf-8')).digest()).lower()

    return pk.decode('utf-8')[:10]


def get_client_ip(request):
    """
    to get client ip request
    :param request:
    :return:
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
